# LK-99 material structure

The LK-99 material has gained a lot of
[popular attention](https://www.tomshardware.com/news/superconductor-breakthrough-replicated-twice)
because finding a room-temperature superconducting
material would be a critical breakthrough for humanity.

The key reference here: https://arxiv.org/pdf/2307.16892.pdf
describes the structure and initial DFT computations.
It cites a structure reference: https://arxiv.org/pdf/2307.12008.pdf

  * "The crystal system of the original lead-apatite is hexagonal (P63/m, 176) with the cell parameters a=9.865 Å and c=7.431 Å . However, LK-99 shows a slight shrinkage compared to the LeadApatite with parameters of a=9.843 Å and c=7.428 Å"

A structural search at [crystallography.net](http://www.crystallography.net/cod/result.php) on elements including Pb, P, O, H with space group number '176'
brings up the [9012631.cif] data file.

## Initial Structural Preparation

- downloaded http://www.crystallography.net/cod/9012631.html

- followed https://pymatgen.org/usage.html to convert cif to coordinates
  (see [result.yaml])

* Note the stoichiometry is `P_6 Pb_10 O_26 H_2 = Pb_10 (PO4)_6 (OH)_2` (agreeing with the 1995 xtal ref., - there's a typo on H-count in the key ref. above)
