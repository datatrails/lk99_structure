function f(x) {
	if(x==15) return "P";
	if(x==29) return "Cu";
	if(x==82) return "Pb";
	if(x==8) return "O";
	return "H";
}

BEGIN {
	Bohr = 0.5291772105638411;
	ax = 9.2350915769650719*Bohr;
	ay = -15.995647823854885*Bohr;
	c  = 13.777993183477045*Bohr;
	a = sqrt(ax*ax+ay*ay);
	v = ax/a # cosine of angle with x-axis
	theta = 2 * 180/3.141592653589793*atan2(sqrt(1-v*v), v);
	printf("CRYST1%9.3f%9.3f%9.3f  90.00  90.00%7.2f P 1           1\n",
	    a, a, c, theta); }

{ n += 1;
  x = ax*($3+$4);
  y = ay*($3-$4);
  z = c*$5;
  printf("ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f\n",
		  n,f($1),"APA"," ",1,x,y,z);
}
